<?php

function pSubscribe($redis, $pattern, $channel, $message)
{
    echo "Pattern: $pattern\n";
    echo "Channel: $channel\n";
    echo "Payload: $message\n";
}

function subscribe($redis, $channel, $message)
{
    echo "Channel: $channel\n";
    echo "Payload: $message\n";
}

function testCallback()
{
    $host = '127.0.0.1';
    $port = 6379;
    $timeout = 0.0;
    $reserved = null;
    $retry_interval = 0;
    $read_timeout = 0;
    $redis = new \Redis();
    $redis->connect($host, $port, $timeout, $reserved, $retry_interval, $read_timeout);
    $redis->subscribe(['redis'], 'subscribe');
}

// testCallback();
