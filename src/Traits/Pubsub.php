<?php

namespace Webdcg\Redis\Traits;

trait Pubsub
{
    /**
     * Subscribe to channels by pattern
     *
     * @param array $patterns  An array of patterns to match
     * @param callable $callback Either a string or an array with an object and method.
     *                           The callback will get four arguments ($redis, $pattern,
     *                           $channel, $message).
     * @return boolean
     *
     * @codeCoverageIgnore
     */
    public function pSubscribe(array $patterns, $callback)
    {
        return $this->redis->pSubscribe($patterns, $callback);
    }
    

    /**
     * Publish messages to channels. Warning: this function will probably change
     * in the future.
     *
     * @param  string $channel  A channel to publish to.
     * @param  string $message  The message to be broadcasted.
     *
     * @return int              the number of clients that received the message.
     */
    public function publish(string $channel, string $message): int
    {
        return $this->redis->publish($channel, $message);
    }


    /**
     * Subscribe to channels. Warning: this function will probably change in the future.
     *
     * @param array $channels       An array of channels to subscribe to
     * @param callable $callback    Either a string or an Array($instance, 'method_name').
     *                              The callback function receives 3 parameters: the redis
     *                              instance, the channel name, and the message. return
     *                              value: Mixed. Any non-null return value in the callback
     *                              will be returned to the caller.
     *
     * @return bool
     *
     * @codeCoverageIgnore
     */
    public function subscribe(array $channels, $callback)
    {
        return $this->redis->subscribe($channels, $callback);
    }


    /**
     * A command allowing you to get information on the Redis pub/sub system.
     *
     * Return Value
     * CHANNELS: Returns an array where the members are the matching channels.
     * NUMSUB: Returns a key/value array where the keys are channel names and
     *          values are their counts.
     * NUMPAT: Integer return containing the number active pattern subscriptions
     *
     * @param string $keyword                   String, which can be: "channels",
     *                                          "numsub", or "numpat"
     * @param [mixed|array|string] $argument    Optional, variant. For the "channels"
     *                                          subcommand, you can pass a string
     *                                          pattern. For "numsub" an array of
     *                                          channel names.
     *
     * @return mixed|array|int
     */
    public function pubSub(string $keyword, $argument = null)
    {
        return is_null($argument) ?
            $this->redis->pubSub($keyword) :
            $this->redis->pubSub($keyword, $argument)
        ;
    }
}
