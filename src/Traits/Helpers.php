<?php

namespace Webdcg\Redis\Traits;

trait Helpers
{
    /**
     * rawCommand
     * A method to execute any arbitrary command against the a Redis server.
     *
     * Parameters
     * This method is variadic and takes a dynamic number of arguments of various
     * types (string, long, double), but must be passed at least one argument
     * (the command keyword itself).
     *
     * Return value
     * The return value can be various types depending on what the server itself
     * returns. No post processing is done to the returned value and must be
     * handled by the client code.
     *
     * @param string $command
     * @param mixed $params
     *
     * @return mixed
     */
    public function rawCommand(string $command, ...$params)
    {
        return $this->redis->rawCommand($command, ...$params);
    }

    
    /**
     * Generate a string with 0s and 1s of the binary representaion of a value.
     *
     * @param  $value
     *
     * @return string   String representation of value with 0s and 1s
     */
    public function getBinaryString($value): string
    {
        return base_convert(unpack('H*', $value)[1], 16, 2);
    }

    
    /**
     * Check that the given array is associative.
     *
     * @param  array   $array
     *
     * @return bool
     */
    public function is_associative(array $array)
    {
        return array_keys($array) !== range(0, count($array) - 1);
    }
}
