<?php

namespace Webdcg\Redis\Traits;

use Webdcg\Redis\Exceptions\BitwiseOperationException;
use Webdcg\Redis\Exceptions\InvalidBitException;

trait Bits
{
    /*
     * Available Bit Operations
     */
    protected $BIT_OPERATIONS = ['AND', 'OR', 'XOR', 'NOT'];


    /**
     * Count set bits in a string.
     *
     * @param  string $key
     *
     * @return int
     */
    public function bitCount(string $key): int
    {
        return $this->redis->bitCount($key);
    }


    /**
     * Perform bitwise operations between strings.
     *
     * @param  string $operation    NOT, AND, OR, XOR
     * @param  string $returnKey    Return Key
     * @param  splat $keys          List of keys for input
     *
     * @return int              The size of the string stored in the destination key.
     */
    public function bitOp(string $operation, string $returnKey, ...$keys): int
    {
        $operation = strtoupper($operation);

        if (!in_array($operation, $this->BIT_OPERATIONS)) {
            throw new BitwiseOperationException('Operation not supported', 1);
        }

        if ('NOT' == $operation) {
            return $this->redis->bitOp($operation, $returnKey, $keys[0]);
        }

        return $this->redis->bitOp($operation, $returnKey, ...$keys);
    }


    /**
     * Sets or clears the bit at offset in the string value stored at key.
     *
     * @param string $key
     * @param int    $offset
     * @param int   $value
     *
     * @return int              0 or 1, the value of the bit before it was set.
     */
    public function setBit(string $key, int $offset, int $value): int
    {
        return $this->redis->setBit($key, $offset, $value);
    }


    /**
     * Returns the bit value at offset in the string value stored at key.
     *
     * @param  string $key
     * @param  int    $offset
     *
     * @return int
     */
    public function getBit(string $key, int $offset): int
    {
        return $this->redis->getBit($key, $offset);
    }


    /**
     * @see https://redis.io/commands/bitfield
     *
     * @return boolean
     */
    public function bitField(): bool
    {
        return false;
    }


    /**
     * bitPos: Return the position of the first bit set to 1 or 0 in a string.
     *
     * The range is interpreted as a range of bytes and not a range of bits,
     * so start=0 and end=2 means to look at the first three bytes.
     *
     * @param string $key
     * @param integer $bit
     * @param integer|null $start
     * @param integer|null $end
     *
     * @return integer
     */
    public function bitPos(string $key, int $bit = 1, ?int $start = null, ?int $end = null): int
    {
        if ($bit < 0 || $bit > 1) {
            throw new InvalidBitException("Invalid value for Bit", 1);
        }

        if (is_null($start) && is_null($end)) {
            return $this->redis->bitPos($key, $bit);
        }

        if (is_null($end)) {
            return $this->redis->bitPos($key, $bit, $start);
        }

        return $this->redis->bitPos($key, $bit, $start, $end);
    }
}
