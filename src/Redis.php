<?php

namespace Webdcg\Redis;

use Webdcg\Redis\Traits\Bits;
use Webdcg\Redis\Traits\Connection;
use Webdcg\Redis\Traits\Geocoding;
use Webdcg\Redis\Traits\Hashes;
use Webdcg\Redis\Traits\Helpers;
use Webdcg\Redis\Traits\HyperLogLogs;
use Webdcg\Redis\Traits\Introspection;
use Webdcg\Redis\Traits\Keys;
use Webdcg\Redis\Traits\Lists;
use Webdcg\Redis\Traits\Pubsub;
use Webdcg\Redis\Traits\Scripting;
use Webdcg\Redis\Traits\Sets;
use Webdcg\Redis\Traits\SortedSets;
use Webdcg\Redis\Traits\Streams;
use Webdcg\Redis\Traits\Strings;
use Webdcg\Redis\Traits\Transactions;

class Redis
{
    use Bits;
    use Connection;
    use Geocoding;
    use Hashes;
    use Helpers;
    use HyperLogLogs;
    use Introspection;
    use Lists;
    use Keys;
    use Pubsub;
    use Scripting;
    use Sets;
    use SortedSets;
    use Streams;
    use Strings;
    use Transactions;

    public const OPT_SERIALIZER = \Redis::OPT_SERIALIZER;
    public const OPT_READ_TIMEOUT = \Redis::OPT_READ_TIMEOUT;
    public const OPT_SCAN = \Redis::OPT_SCAN;
    public const SERIALIZER_NONE = \Redis::SERIALIZER_NONE;
    public const SCAN_RETRY = \Redis::SCAN_RETRY;
    public const REDIS_STRING = \Redis::REDIS_STRING;

    /**
     * Redis Instance
     *
     * @var \Redis
     */
    protected $redis;

    /**
     * Redis
     */
    public function __construct()
    {
        $this->redis = new \Redis();
    }
}
