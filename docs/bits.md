### [Redis client for PHP using the PhpRedis C Extension](../README.md)

# [Bits](docs/bits.md)

|Command                |Description                                                        |Supported              |Tested                 |Class/Trait    |Method     |
|---                    |---                                                                |:-:                    |:-:                    |---            |---        |
|[bitCount](#bitcount)  |Count set bits in a string                                         |:white\_check\_mark:   |:white\_check\_mark:   |Bits           |bitCount   |
|[bitField](#bitfield)  |Perform arbitrary bitfield integer operations on strings           |:x:                    |:x:                    |Bits           |bitField   |
|[bitOp](#bitop)        |Perform bitwise operations between strings                         |:white\_check\_mark:   |:white\_check\_mark:   |Bits           |bitOp      |
|[bitPos](#bitpos)      |Find first bit set or clear in a string                            |:white\_check\_mark:   |:white\_check\_mark:   |Bits           |bitPos     |
|[getBit](#getbit)      |Returns the bit value at offset in the string value stored at key  |:white\_check\_mark:   |:white\_check\_mark:   |Bits           |getBit     |
|[setBit](#setbit)      |Sets or clears the bit at offset in the string value stored at key |:white\_check\_mark:   |:white\_check\_mark:   |Bits           |setBit     |


## Usage

```php
$redis = new Webdcg\Redis\Redis;

$redis->bitCount($key);
$redis->bitField($key);
$redis->bitPos($key, $bit, $start, $end); // $bit 1 | 0
$redis->bitOp($operation, $destinationKey, $key, $keyOptional); // $operation ['not', 'or', 'and']
$redis->getBit($key, $offset);
$redis->setBit($key, $offset, $value); // $value 1 | 0
```



## bitCount

_**Description**_: Count set bits in a string.

##### *Prototype*  

```php
public function bitCount(string $key) : int {
    return $redis->bitCount($key);
}
```

##### *Parameters*

- *key*: String. The key append to.

##### *Return value*

*int*: Total of bits set in the string.

##### *Example*

```php
$redis->set('key', 'a'); // 1100001 in Binary or 97 in decimal
$redis->bitCount('key'); // 3
```



## bitField

_**Description**_: Perform arbitrary bitfield integer operations on strings

##### *Prototype*  

```php
public function bitField(): bool {
    return false;
}
```

##### *Parameters*

- *key*: String. The key append to.

##### *Return value*

*int*: Total of bits set in the string.

##### *Example*

```php
$redis->set('key', 'a');
$redis->bitCount('key'); // 3
```



## bitOp

_**Description**_: Perform bitwise operations between strings.

##### *Prototype*  

```php
public function bitOp(string $operation, string $returnKey, ...$keys): int {
    return $redis->bitOp($key);
}
```

##### *Parameters*

- *operation*: String. Bitwise operation to perform: AND, OR, NOT, XOR
- *returnKey*: String. The key where the result will be saved..
- *keys*: String(s). The key(s) part of he operation.

##### *Return value*

*int*: The size of the string stored in the destination key.

##### *Example*

```php
$redis->setBit($key, 0, 0)); // 00000000
$redis->setBit($key, 2, 1)); // 00100000
$redis->setBit($keyOptional, 0, 1)); // 10000000
$redis->setBit($keyOptional, 2, 0)); // 10100000
$redis->bitOp('or', 'testBitOpOr', $key, $keyOptional);
$testBitOpOr = $redis->getBinaryString($redis->get('testBitOpOr')); // 10100000
```



## bitPos

_**Description**_: Return the position of the first bit set to 1 or 0 in a string.

##### *Prototype*  

```php
public function bitPos(string $key, int $bit = 1, ?int $start = null, ?int $end = null): int {
    if (is_null($start) && is_null($end)) {
        return $redis->bitPos($key, $bit);
    }
    if (is_null($end)) {
        return $redis->bitPos($key, $bit, $start);
    }
    return $redis->bitPos($key, $bit, $start, $end);
}
```

##### *Parameters*

- *key*: String. The key to Search.
- *bit*: Int. 0 or 1 Bit to look for.
- *start*: Int. Byte number to start range.
- *end*: Int. Byte number to end range.

##### *Return value*

*int*: The command returns the position of the first bit set to 1 or 0 according to the request.

##### *Example*

```php
$redis->set($key, "\xff\xf0\x00"); // 
// 11111111 11110000 00000000
//              ↑
$redis->bitPos($key, 0); // 12
```



## setBit

_**Description**_: Sets or clears the bit at offset in the string value stored at key.

##### *Prototype*  

```php
public function setBit(string $key, int $offset, int $value): int {
    return $redis->setBit($key, $offset, $value);
}
```

##### *Parameters*

- *operation*: String. The key append to.
- *returnKey*: String. The key append to.
- *keys*: String. The key append to.

##### *Return value*

*int*: Total of bits set in the string.

##### *Example*

```php
$redis->setBit('key', 1, 1);
$redis->setBit('key', 7, 1);
$redis->get('key'); // A => 01000001
```



## getBit

_**Description**_: Returns the bit value at offset in the string value stored at key.

##### *Prototype*  

```php
public function getBit(string $key, int $offset) : int {
    return $redis->getBit($key, $offset);
}
```

##### *Parameters*

- *key*: String. The Bitmap.
- *offset*: String. The bit position within the string.

##### *Return value*

*int*: 0 or 1

##### *Example*

```php
$redis->set('key', 'A'); // 01000001
$redis->getBit('key', 0); // 0
$redis->getBit('key', 1); // 1
```
