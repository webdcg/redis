<?php

namespace Webdcg\Redis\Tests;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Process\PhpProcess;
use Webdcg\Redis\Redis;

class RedisPubSubTest extends TestCase
{
    protected $redis;
    protected $key;
    protected $keyOptional;
    protected $producer;

    protected function setUp(): void
    {
        $this->redis = new Redis();
        $this->redis->connect();
        $this->redis->setOption(\Redis::OPT_SERIALIZER, \Redis::SERIALIZER_NONE);
        $this->key = 'PubSub';
        $this->keyOptional = 'PubSub:Optional';
    }

    /** @test */
    public function redis_PubSub_publish_single()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));
        $this->assertEquals(0, $this->redis->publish($this->key, 'Hello Redis..!'));
    }

    /** @test */
    public function redis_PubSub_pSubscribe()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));
        $this->publish();
        // $this->redis->pSubscribe([$this->key], 'pSubscribe');
    }

    /** @test */
    public function redis_PubSub_subscribe()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));
        $this->publish();
        // $this->redis->subscribe([$this->key], 'subscribe');
    }

    /** @test */
    public function redis_PubSub_PubSub_channels()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));
        $this->publish(10);
        $channels = $this->redis->pubSub('channels');
        $this->assertIsArray($channels);
        $this->assertIsIterable($channels);
    }

    /** @test */
    public function redis_PubSub_PubSub_channels_pattern()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));
        $this->publish(1000);
        $channels = $this->redis->pubSub('channels', 'Pub*');
        $this->assertIsArray($channels);
        $this->assertIsIterable($channels);
        // $this->assertContains($this->key, $channels);
    }

    /** @test */
    public function redis_PubSub_PubSub_numsub()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));
        $this->publish(100);
        $numsub = $this->redis->pubSub('numsub', [$this->key]);
        $this->assertArrayHasKey($this->key, $numsub);
        $this->assertEquals([$this->key => 0], $numsub);
    }

    /** @test */
    public function redis_PubSub_PubSub_numpat()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));
        $this->publish(100);
        $numpat = $this->redis->pubSub('numpat', [$this->key]);
        $this->assertIsInt($numpat);
        $this->assertIsScalar($numpat);
        $this->assertEquals(0, $numpat);
    }

    /**
     * ========================================================================
     * H E L P E R   M E T H O D S
     * ========================================================================
     */

    /**
     * Queue Producer
     * Using the Symfony Process component, we connect to Redis and create
     * a single element on a Queue.
     * See: https://symfony.com/doc/current/index.html#gsc.tab=0.
     *
     * @return
     */
    protected function publish(int $number = 1)
    {
        $this->producer = new PhpProcess(
            <<<EOF
<?php
require __DIR__ . '/vendor/autoload.php';
use Webdcg\Redis\Redis;
\$redis = new Redis();
\$redis->connect();
usleep(1000 * random_int(50, 100));
for(\$i = 0; \$i < {$number}; \$i++) {
    \$redis->publish('{$this->key}', 'redis-'.\$i);
}
EOF
        );
        $this->producer->run();
    }
}
