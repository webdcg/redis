<?php

namespace Webdcg\Redis\Tests;

use PHPUnit\Framework\TestCase;
use Webdcg\Redis\Redis;

/**
 * RedisBitsTest
 */
class RedisBitsTest extends TestCase
{
    /**
     * Redis instance
     *
     * @var Webdcg\Redis\Redis
     */
    protected $redis;

    /**
     * @var string
     */
    protected $key = '';

    /**
     * @var string
     */
    protected $keyOptional = '';

    
    /**
     * Initialize Redis object and keys
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->redis = new Redis();
        $this->redis->connect();
        $this->key = 'Bits';
        $this->keyOptional = 'BitsOptional';
    }


    /** @test */
    public function redis_bits_bitcount()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));

        // Prepare
        $this->assertTrue($this->redis->set($this->key, 'a'));
        $value = $this->redis->get($this->key);

        // Test
        $this->assertEquals(97, ord($value));
        $this->assertEquals('1100001', $this->redis->getBinaryString($value));
        $this->assertEquals(3, $this->redis->bitCount($this->key));

        // Cleanup
        $this->assertEquals(1, $this->redis->delete($this->key));
        $this->assertEquals(0, $this->redis->exists($this->key));
    }


    /** @test */
    public function redis_bits_bitop()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));

        // Prepare
        $this->assertGreaterThanOrEqual(0, $this->redis->setBit($this->key, 0, 0));
        $this->assertGreaterThanOrEqual(0, $this->redis->setBit($this->key, 2, 1));
        $this->assertGreaterThanOrEqual(0, $this->redis->setBit($this->keyOptional, 0, 1));
        $this->assertGreaterThanOrEqual(0, $this->redis->setBit($this->keyOptional, 2, 0));

        // Test
        $this->assertEquals(1, $this->redis->bitOp('or', 'testBitOpOr', $this->key, $this->keyOptional));
        $testBitOpOr = $this->redis->getBinaryString($this->redis->get('testBitOpOr'));
        $this->assertEquals('10100000', $testBitOpOr);

        // Cleanup
        $this->assertEquals(1, $this->redis->delete($this->key));
        $this->assertEquals(0, $this->redis->exists($this->key));
    }


    /** @test */
    public function redis_bits_bitpos()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));

        // Prepare
        $this->assertEquals(1, $this->redis->set($this->key, "\xff\xf0\x00"));

        // Test
        // 11111111 11110000 00000000
        //              ↑
        $this->assertEquals(12, $this->redis->bitPos($this->key, 0));

        // Remove all the keys used
        $this->assertEquals(1, $this->redis->delete($this->key));
    }


    /** @test */
    public function redis_bits_setbit()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));

        // Test
        $this->assertEquals(0, $this->redis->setBit($this->key, 1, 1));
        $this->assertEquals(0, $this->redis->setBit($this->key, 7, 1));
        $this->assertEquals('A', $this->redis->get($this->key)); // 01000001

        // Remove all the keys used
        $this->assertEquals(1, $this->redis->delete($this->key));
    }


    /** @test */
    public function redis_bits_getbit()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));

        // A ASCII 65 01000001
        $this->assertTrue($this->redis->set($this->key, 'A'));
        $this->assertEquals('A', $this->redis->get($this->key));
        $this->assertEquals(0, $this->redis->getBit($this->key, 0));
        $this->assertEquals(1, $this->redis->getBit($this->key, 1));

        // Remove all the keys used
        $this->assertEquals(1, $this->redis->delete($this->key));
    }


    /** @test */
    public function redis_bits_bitfield()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));
        
        // Remove all the keys used
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));
    }
}
