<?php

namespace Webdcg\Redis\Tests;

use PHPUnit\Framework\TestCase;
use Webdcg\Redis\Redis;

/**
 * setBitTest
 *
 * Redis | Bits | setBit => Changes a single bit of a string.
 */
class setBitTest extends TestCase
{
    protected $redis;
    protected $key;
    protected $keyOptional;
    protected $group;
    protected $producer;


    /**
     * setBitTest
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->redis = new Redis();
        $this->redis->connect();
        $this->redis->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_NONE);
        $this->key = 'Bits:setBitTest';
        $this->keyOptional = $this->key . ':Optional';
        $this->group = $this->key . ':Group';
    }


    /** @test */
    public function redis_bits_setbit()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));

        // Prepare
        // A ASCII 65 01000001
        $this->assertTrue($this->redis->set($this->key, 'A'));

        // Test
        // Modify the second bit, it was 0 previously
        $this->assertEquals(0, $this->redis->setBit($this->key, 2, 1));
        // a ASCII 97 01100001
        $this->assertEquals('a', $this->redis->get($this->key));

        // Remove all the keys used
        $this->assertEquals(1, $this->redis->delete($this->key));
    }
}
