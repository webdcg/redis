<?php

namespace Webdcg\Redis\Tests;

use PHPUnit\Framework\TestCase;
use Webdcg\Redis\Redis;

/**
 * getBitTest
 *
 * Redis | Bits | getBit => Returns the bit value at offset in the string value stored at key.
 */
class getBitTest extends TestCase
{
    protected $redis;
    protected $key;
    protected $keyOptional;
    protected $group;
    protected $producer;


    /**
     * getBitTest
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->redis = new Redis();
        $this->redis->connect();
        $this->redis->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_NONE);
        $this->key = 'Bits:getBitTest';
        $this->keyOptional = $this->key . ':Optional';
        $this->group = $this->key . ':Group';
    }


    /** @test */
    public function redis_bits_getbit()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));

        // A ASCII 65 01000001
        $this->assertTrue($this->redis->set($this->key, 'A'));
        $this->assertEquals('A', $this->redis->get($this->key));
        $this->assertEquals(0, $this->redis->getBit($this->key, 0));
        $this->assertEquals(1, $this->redis->getBit($this->key, 1));
        $this->assertEquals(0, $this->redis->getBit($this->key, 2));
        $this->assertEquals(0, $this->redis->getBit($this->key, 3));
        $this->assertEquals(0, $this->redis->getBit($this->key, 4));
        $this->assertEquals(0, $this->redis->getBit($this->key, 5));
        $this->assertEquals(0, $this->redis->getBit($this->key, 6));
        $this->assertEquals(1, $this->redis->getBit($this->key, 7));

        // Remove all the keys used
        $this->assertEquals(1, $this->redis->delete($this->key));
    }
}
