<?php

namespace Webdcg\Redis\Tests;

use PHPUnit\Framework\TestCase;
use Webdcg\Redis\Redis;

/**
 * bitCountTest
 *
 * Redis | Bits | bitCount => Count bits in a string.
 */
class bitCountTest extends TestCase
{
    protected $redis;
    protected $key;
    protected $keyOptional;
    protected $group;
    protected $producer;


    /**
     * bitCountTest
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->redis = new Redis();
        $this->redis->connect();
        $this->redis->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_NONE);
        $this->key = 'Bits:bitCountTest';
        $this->keyOptional = $this->key . ':Optional';
        $this->group = $this->key . ':Group';
    }


    /** @test */
    public function redis_bits_bitcount_lowercase()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));
        
        // Prepare
        $this->assertTrue($this->redis->set($this->key, 'a'));
        $value = $this->redis->get($this->key);

        // Test
        $this->assertEquals(97, ord($value));
        $this->assertEquals('1100001', $this->redis->getBinaryString($value));
        $this->assertEquals(3, $this->redis->bitCount($this->key));
        $this->assertEquals(1, $this->redis->delete($this->key));
        $this->assertEquals(0, $this->redis->exists($this->key));
    }


    /** @test */
    public function redis_bits_bitcount_uppercase()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));

        // Prepare
        $this->assertTrue($this->redis->set($this->key, 'A'));
        $value = $this->redis->get($this->key);

        // Test
        $this->assertEquals(65, ord($value));
        $this->assertEquals('1000001', $this->redis->getBinaryString($value));
        $this->assertEquals(2, $this->redis->bitCount($this->key));
        $this->assertEquals(1, $this->redis->delete($this->key));
        $this->assertEquals(0, $this->redis->exists($this->key));
    }


    /** @test */
    public function redis_bits_bitcount_fixed()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));

        // Prepare
        for ($i = 0;$i < 10;$i++) {
            if ($i % 2) {
                $this->assertEquals(0, $this->redis->setBit($this->key, $i, 1));
            }
        }
        
        $value = $this->redis->get($this->key);

        // Test
        $this->assertEquals(85, ord($value));
        $this->assertEquals('101010101000000', $this->redis->getBinaryString($value));
        $this->assertEquals(5, $this->redis->bitCount($this->key));
        $this->assertEquals(1, $this->redis->delete($this->key));
        $this->assertEquals(0, $this->redis->exists($this->key));
    }


    /** @test */
    public function redis_bits_bitcount_random()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));

        // Prepare
        for ($i = 0;$i < 10;$i++) {
            if (rand(0, 100) > 49) {
                $this->assertEquals(0, $this->redis->setBit($this->key, $i, 1));
            }
        }
        
        $value = $this->redis->get($this->key);

        // Test
        $this->assertGreaterThanOrEqual(1, ord($value));
        $this->assertGreaterThanOrEqual(1, $this->redis->bitCount($this->key));
        $this->assertEquals(1, $this->redis->delete($this->key));
        $this->assertEquals(0, $this->redis->exists($this->key));
    }
}
