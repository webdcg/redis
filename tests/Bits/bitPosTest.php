<?php

namespace Webdcg\Redis\Tests;

use PHPUnit\Framework\TestCase;
use Webdcg\Redis\Exceptions\InvalidBitException;
use Webdcg\Redis\Redis;

/**
 * bitPosTest
 *
 * Redis | Bits | bitPos => Return the position of the first bit set to 1 or 0 in a string.
 */
class bitPosTest extends TestCase
{
    protected $redis;
    protected $key;
    protected $keyOptional;
    protected $group;
    protected $producer;


    /**
     * bitPosTest
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->redis = new Redis();
        $this->redis->connect();
        $this->redis->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_NONE);
        $this->key = 'Bits:bitPosTest';
        $this->keyOptional = $this->key . ':Optional';
        $this->group = $this->key . ':Group';
    }


    /** @test */
    public function redis_bits_bitpos_first_zero()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));

        // Prepare
        $this->assertEquals(1, $this->redis->set($this->key, "\xff\xf0\x00"));

        // Test
        // 11111111 11110000 00000000
        //              ↑
        $this->assertEquals(12, $this->redis->bitPos($this->key, 0));

        // Remove all the keys used
        $this->assertEquals(1, $this->redis->delete($this->key));
    }

    
    /** @test */
    public function redis_bits_bitpos_first_one_second_byte()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));

        // Prepare
        $this->assertEquals(1, $this->redis->set($this->key, "\xff\xf0\x00"));

        // Test
        // 11111111 11110000 00000000
        //          ↑
        $this->assertEquals(8, $this->redis->bitPos($this->key, 1, 1));

        // Remove all the keys used
        $this->assertEquals(1, $this->redis->delete($this->key));
    }


    /** @test */
    public function redis_bits_bitpos_first_one_from_start()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));

        // Prepare
        $this->assertEquals(1, $this->redis->set($this->key, "\x00\xff\xf0"));

        // Test
        // 00000000 11111111 11110000
        //          ↑
        $this->assertEquals(8, $this->redis->bitPos($this->key, 1, 0));

        // Remove all the keys used
        $this->assertEquals(1, $this->redis->delete($this->key));
    }


    /** @test */
    public function redis_bits_bitpos_first_one_third_byte()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));

        // Prepare
        $this->assertEquals(1, $this->redis->set($this->key, "\x00\xff\xf0"));

        // Test
        // 00000000 11111111 11110000
        //                   ↑
        $this->assertEquals(16, $this->redis->bitPos($this->key, 1, 2));

        // Remove all the keys used
        $this->assertEquals(1, $this->redis->delete($this->key));
    }


    /** @test */
    public function redis_bits_bitpos_first_zero_between_second_and_third_byte()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));

        // Prepare
        $this->assertEquals(1, $this->redis->set($this->key, "\xff\xf0\xff\x00"));

        // Test
        // 11111111 11110000 11111111 00000000
        //              ↑
        $this->assertEquals(12, $this->redis->bitPos($this->key, 0, 1, 2));

        // Remove all the keys used
        $this->assertEquals(1, $this->redis->delete($this->key));
    }


    /** @test */
    public function redis_bits_invalid_search()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));

        // Prepare
        $this->assertEquals(1, $this->redis->set($this->key, "\x00\xff\xf0"));

        // Test
        $this->expectException(InvalidBitException::class);
        $this->assertEquals(12, $this->redis->bitPos($this->key, 2));

        // Remove all the keys used
        $this->assertEquals(1, $this->redis->delete($this->key));
    }

    
    /** @test */
    public function redis_bits_bitpos_first_one()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));

        // Prepare
        $this->assertEquals(1, $this->redis->set($this->key, "\x00\x00\x00"));

        // Test
        // 00000000 00000000 00000000
        //              ↑
        $this->assertEquals(-1, $this->redis->bitPos($this->key, 1));

        // Remove all the keys used
        $this->assertEquals(1, $this->redis->delete($this->key));
    }
}
