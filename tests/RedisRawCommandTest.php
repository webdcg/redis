<?php

namespace Webdcg\Redis\Tests;

use PHPUnit\Framework\TestCase;
use Webdcg\Redis\Redis;

/**
 * RedisRawCommandTest
 */
class RedisRawCommandTest extends TestCase
{
    /**
     * Redis instance
     *
     * @var Webdcg\Redis\Redis
     */
    protected $redis;

    /**
     * @var string
     */
    protected $key = '';

    /**
     * @var string
     */
    protected $keyOptional = '';


    /**
     * Initialize objects and keys
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->redis = new Redis();
        $this->redis->connect();
        $this->redis->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_NONE);
        $this->redis->select(0);
        $this->key = 'RawCommand';
        $this->keyOptional = 'RawCommand:Optional';
    }


    /**
     * Redis | RawCommand | Bits => bitCount
     *
     * @test
     */
    public function redis_RawCommand_bitCount()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));
        
        // Prepare
        $this->assertTrue($this->redis->set($this->key, 'a'));
        
        // Test
        $this->assertEquals(3, $this->redis->rawCommand('bitCount', $this->key));
        $this->assertEquals($this->redis->bitCount($this->key), $this->redis->rawCommand('bitCount', $this->key));
        
        // Cleanup
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));
    }


    /**
     * Redis | RawCommand | Connection => ping
     *
     * @test
     */
    public function redis_RawCommand_ping()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));
        
        // Test
        $this->assertTrue($this->redis->rawCommand('ping'));
        $this->assertEquals($this->key, $this->redis->rawCommand('ping', $this->key));
        $this->assertEquals($this->redis->ping($this->key), $this->redis->rawCommand('ping', $this->key));
        
        // Cleanup
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));
    }


    /**
     * Redis | RawCommand | GeoCoding => geoAdd
     *
     * @test
     */
    public function redis_RawCommand_geoAdd()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));

        // Prepare
        $longitude = -122.431;
        $latitude = 37.773;
        $location = 'San Francisco';

        // Test
        $this->assertEquals(1, $this->redis->rawCommand('geoAdd', $this->key, $longitude, $latitude, $location));
        $this->assertEquals(0, $this->redis->geoAdd($this->key, $longitude, $latitude, $location));

        // Cleanup
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));
    }


    /**
     * Redis | RawCommand | Hash => hSet
     *
     * @test
     */
    public function redis_RawCommand_hSet()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));

        // Prepare
        $hash = [
            'tswift' => 'Taylor Swift',
            'millaj' => 'Milla Jovovich',
        ];

        // Test
        $this->assertEquals(1, $this->redis->rawCommand('hSet', $this->key, 'tswift', 'Taylor Swift'));
        $this->assertEquals(1, $this->redis->hSet($this->key, 'millaj', 'Milla Jovovich'));
        $this->assertSame($hash, $this->redis->hGetAll($this->key));

        // Cleanup
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));
    }


    /**
     * Redis | RawCommand | HyperLogLog => pfAdd
     *
     * @test
     */
    public function redis_RawCommand_pfAdd()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));

        // Test
        $this->assertEquals(1, $this->redis->rawCommand('pfAdd', $this->key, 'a', 'b', 'c'));
        $this->assertEquals(0, $this->redis->pfAdd($this->key, ['a', 'c']));

        // Cleanup
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));
    }


    /**
     * Redis | RawCommand | Keys => type
     *
     * @test
     */
    public function redis_RawCommand_type()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));

        // Prepare
        $this->assertTrue($this->redis->set($this->key, 'value'));

        // Test
        $this->assertEquals(Redis::REDIS_STRING, $this->redis->rawCommand('type', $this->key));
        $this->assertEquals(Redis::REDIS_STRING, $this->redis->type($this->key));

        // Cleanup
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));
    }


    /**
     * Redis | RawCommand | List => rPush, lLen, lRange
     *
     * @test
     */
    public function redis_RawCommand_rPush()
    {
        // Start from scratch
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));

        // Test
        $this->assertEquals(3, $this->redis->rawCommand('rPush', $this->key, 'one', 2, 3.3));
        $this->assertEquals(3, $this->redis->rawCommand('lLen', $this->key));
        $this->assertEquals(['one', 2, 3.3], $this->redis->rawCommand('lRange', $this->key, 0, -1));

        // Cleanup
        $this->assertGreaterThanOrEqual(0, $this->redis->delete($this->key));
    }
}
